openapi: 3.0.3

info:
  title: Jivetel API
  version: 0.0.1

servers:
  - url: https://portal.jivetel.net/

security:
  - oAuth: [ ]

paths:
  /ns-api/oauth2/token:
    post:
      summary: Authenticate and get an access token
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                grant_type:
                  type: string
                  enum:
                    - password
                client_id:
                  type: string
                client_secret:
                  type: string
                username:
                  type: string
                password:
                  type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                properties:
                  access_token:
                    type: string
                  expires_in:
                    type: integer
                    description: Seconds until token expires

  /ns-api:
    post:
      summary: Manage event subscriptions
      security:
        - oAuth: [ ]
      parameters:
        - name: object
          in: query
          required: true
          schema:
            type: string
            enum: [ "event" ]
        - name: action
          in: query
          required: true
          schema:
            type: string
            enum: [ "create", "read" ]
            description: Specifies whether to create a new subscription or retrieve the list of existing subscriptions
        - name: format
          in: query
          required: true
          schema:
            type: string
            enum: [ "json" ]
        - name: Authorization
          in: header
          required: true
          schema:
            type: string
            pattern: 'Bearer \s+'
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              oneOf:
                - $ref: '#/components/schemas/CreateSubscriptionRequest'
                - $ref: '#/components/schemas/ReadSubscriptionsRequest'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: '#/components/schemas/CreateSubscriptionResponse'
                  - $ref: '#/components/schemas/ReadSubscriptionsResponse'

      callbacks:
        call:
          '{$request.body#/post_url}':
            post:
              requestBody:
                content:
                  application/json:
                    schema:
                      type: array
                      items:
                        $ref: "#/components/schemas/Call"
              responses: { }

components:
  securitySchemes:
    oAuth:
      type: oauth2
      flows:
        password:
          tokenUrl: /ns-api/oauth2/token
          scopes: { }
      in: header
      name: Authorization

  schemas:
    Call:
      required:
        - orig_id
        - orig_req_user
        - orig_type
        - sessionId
        - term_call_info
        - term_id
        - time_begin
      properties:
        by_action:
          type: string
          example: "CallRequest"
        by_sub:
          type: string
          description: The subscriber (extension or phone) that answered
        orig_id:
          type: string
          description: Caller ID number
          format: phone
        orig_name:
          type: string
          description: Caller ID name
          format: phone
        orig_req_user:
          type: string
          description: The number the caller dialed
          format: phone
        orig_type:
          type: string
          example: "device"
        remove:
          type: string
          enum: [ "yes" ]
          required: false
          description: If `"yes"`, indicates that the call has completed
        sessionId:
          type: string
          description: Uniquely identifies the call session
        term_call_info:
          type: string
          enum: [ held, alerting, active ]
          description: >
            The current state of the call. Possible values are:
              * `"held"`: The call is on hold
              * `"alerting"`: The call is ringing
              * `"active"`: The call is active
        term_id:
          type: string
          description: The number the call was answered on
          format: phone
        time_begin:
          $ref: '#/components/schemas/LocalDateTime'
        time_answer:
          oneOf:
            - type: string
              enum: [ "0000-00-00 00:00:00" ]
              description: The call has not been answered
            - $ref: '#/components/schemas/LocalDateTime'

    CreateSubscriptionRequest:
      required: [ domain, model, post_url ]
      properties:
        domain:
          type: string
          description: The Jivetel domain to receive events for
        model:
          type: string
          description: The type of event object to receive
          enum:
            - call
            - call_origid
        post_url:
          type: string
          description: The URL to post events to
          format: uri
          example: https://myserver.com/send/callback/here

    ReadSubscriptionsRequest:
      required: [ domain ]
      properties:
        domain:
          type: string
          description: The Jivetel domain to get subscriptions for

    CreateSubscriptionResponse:
      allOf:
        - type: object
          required: [ id ]
          properties:
            id:
              type: string
              description: The ID of the subscription
        - $ref: '#/components/schemas/_BaseSubscriptionResponse'

    ReadSubscriptionsResponse:
      type: array
      items:
        type: object
        allOf:
          - type: object
            required: [ subscription_id ]
            properties:
              subscription_id:
                type: string
                description: The ID of the subscription
          - $ref: '#/components/schemas/_BaseSubscriptionResponse'

    _BaseSubscriptionResponse:
      required: [ creation, expires ]
      properties:
        creation:
          $ref: '#/components/schemas/LocalDateTime'
        expires:
          $ref: '#/components/schemas/LocalDateTime'

    LocalDateTime:
      type: string
      format: date-time
      pattern: '^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$'
      example: '2019-01-01 00:00:00'
